package com.zlzlib.librx.demo;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import com.zlzlib.librx.BaseProgressDialogHandler;
import com.zlzlib.librx.listener.RxCancelListener;

/**
 * Created by zlz on 2019/8/20.
 */
public class DemoProgressHandler extends BaseProgressDialogHandler {

    private ProgressDialog dialog;

    public DemoProgressHandler(Context context, RxCancelListener listener, boolean cancelable) {
        super(context, listener, cancelable);
        dialog = new ProgressDialog(context);
        dialog.setTitle("loading...");
    }

    @Override
    protected Dialog getDialog() {
        return dialog;
    }

    @Override
    protected void setDialogNull() {
        dialog = null;
    }

    @Override
    protected void setProgressMessage(String msg) {
        if (null != dialog) {
            dialog.setMessage(msg);
        }
    }

}