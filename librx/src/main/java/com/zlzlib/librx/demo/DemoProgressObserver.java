package com.zlzlib.librx.demo;


import android.content.Context;

import com.zlzlib.librx.BaseProgressDialogHandler;
import com.zlzlib.librx.BaseProgressObserver;


/**
 * Created by zlz on 2019/8/22.
 */
public abstract class DemoProgressObserver<T> extends BaseProgressObserver<T> {

    public DemoProgressObserver(Context context) {
        super(context, true);
    }

    @Override
    protected BaseProgressDialogHandler dialogHandler(Context context, boolean cancelable) {
        return new DemoProgressHandler(context, this, cancelable);
    }

}
