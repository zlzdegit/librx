package com.zlzlib.librx;


import com.zlzlib.librx.listener.RxCancelListener;

import io.reactivex.rxjava3.observers.DisposableObserver;

/**
 * Created by zlz on 2019/8/23.
 */
public abstract class BaseObserver<T> extends DisposableObserver<T> implements RxCancelListener {


    /**
     * 取消对observable的订阅，
     */
    @Override
    public void onCancelDisposable() {
        dispose();
    }

}
