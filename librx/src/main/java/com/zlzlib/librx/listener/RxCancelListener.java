package com.zlzlib.librx.listener;

/**
 * Created by zlz on 2019/8/20.
 * 取消订阅
 */
public interface RxCancelListener {

    void onCancelDisposable();

}
