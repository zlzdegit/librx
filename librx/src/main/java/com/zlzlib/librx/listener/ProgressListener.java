package com.zlzlib.librx.listener;


import com.zlzlib.librx.ProgressInfo;

/**
 * 上传和下载进度回调
 * Created by zlz on 2019/8/20.
 */
public interface ProgressListener<T> {

    //更新上传进度
    void updateUp(ProgressInfo<T> info);
    //更新下载进度
    void updateDown(ProgressInfo<T> info);

}
